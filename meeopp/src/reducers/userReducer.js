import { SET_USER_DATA } from '../constants/actionTypes';
const initialState = {
    userData: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            console.log('setUserData', action.type);
            return {
                ...state,
                userData: action.userData,
            };
        default:
            return state
    }
}
