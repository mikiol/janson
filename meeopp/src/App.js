import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setUserData } from './actions/simpleAction.js';
import { postUserData, getUserData } from './service';

import './App.css';

class App extends Component {

  state = {
    firstName: '',
    lastName: '',
    company: '',
    department: '',
    position: '',
    email: '',
    isSaved: false
  }
 
  componentWillMount() {
    getUserData(this.props.dispatch).then((data)=>{
      this.setState({...data});
    })
  }
  

  onChange = (event, name) => {
    console.log('name ', event.target.value, ' value ');
    this.setState({
      [name]: event.target.value
    })
  }

  handleSubmit = (event) => {
    console.log(this.state);
    const { dispatch } = this.props;
    event.preventDefault();
    dispatch(setUserData(this.state));
    postUserData(dispatch, this.state).then(
      isSaved => { 
        console.log('isSaved', isSaved);
      if (isSaved === 'saved') {
          this.setState({
            isSaved: true
          })

          setTimeout(() => {
            this.setState({
              isSaved: false
            });
          }, 5000)
        }
      }
    )
  }

  render() {

    return (
      <div className="wrapper">
        <div className="container">
          <h1>Welcome</h1>
          <form className="form">
            <div className="flex_wrapper">
                <label>First Name</label>
                <input type="text" value={this.state.firstName }  placeholder="First Name" onChange={(e) => this.onChange(e, 'firstName')}/>
            </div>
            <div className="flex_wrapper">
              <label>Last Name</label>
              <input type="text" value={this.state.lastName } placeholder="Last Name" onChange={(e) => this.onChange(e, 'lastName')} />
            </div>
            
            <div className="flex_wrapper">
              <label>Department</label>
              <input type="text" value={this.state.department } placeholder="Department" onChange={(e) => this.onChange(e, 'department')} />
            </div>

            <div className="flex_wrapper">
              <label>Company</label>
              <input type="text" value={ this.state.company } placeholder="Company" onChange={(e) => this.onChange(e, 'company')} />
            </div>


            <div className="flex_wrapper">
              <label>Position</label>
              <input type="text" value={this.state.position } placeholder="Position" onChange={(e) => this.onChange(e, 'position')} />
            </div>

            <div className="flex_wrapper">
              <label>Email</label>
              <input type="text" value={this.state.email} placeholder="Email" onChange={(e) => this.onChange(e, 'email')} />
            </div>

            <div className="flex_wrapper" style={{marginTop: 30}}>
            <label></label>
            <button type="submit" id="login-button" onClick={this.handleSubmit} >Login</button>
            </div>
            { this.state.isSaved && (<p style={{textAlign: 'center'}}>Successfull saved!</p>)}
          </form>
        </div>
        <ul className="bg-bubbles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userData: state.user.userData
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
