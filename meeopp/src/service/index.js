import axios from 'axios';
import { setUserData } from '../actions/simpleAction';
export const postUserData = (dispatch, body) =>
    axios.post(`http://localhost:3001/api/user`, 
            {body}
        )
        .then(data => {
            if (data && data.status === 200) {
                console.log('saved');
                return 'saved';
            }
        });


export const getUserData = (dispatch) => 
        axios.get('http://localhost:3001/api/user')
        // .then(res => res.json())
        // .catch( err => console.log(err))
        .then(data => {
            console.log(data);
            if (data && data.status === 200) {
                console.log('received ', data);
                dispatch(setUserData(data.data));
                return data.data;
            }
        })